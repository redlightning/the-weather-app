package cz.martykan.forecastie.weatherapi.rainviewer

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.google.gson.Gson
import com.google.gson.JsonParser
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.FileOutputStream
import java.io.InputStreamReader
import java.net.URL

class RainViewerJsonWorker (val context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {
    private val url = "https://api.rainviewer.com/public/weather-maps.json"

    override fun doWork(): Result {
        var apiResponse = ""
        try {
            apiResponse = URL(url).readText()
            val gson = Gson()
            val ob = gson.fromJson(apiResponse, RainViewerAPI::class.java)
//            Log.i(TAG, "OMG IT WORKED!!!!")
//            Log.i(TAG, ob.host + ob.radar!!.past[0].path!!)
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
            return Result.retry()
        }
        val outputData = workDataOf(KEY_JSON to apiResponse)
        return Result.success(outputData)
    }

    companion object {
        const val TAG = "RadarListDownloadWorker"
        const val KEY_JSON = "json"
    }
}