package cz.martykan.forecastie.weatherapi.rainviewer

/**
 * @url https://www.rainviewer.com/api/weather-maps-api.html
 */
class RainViewerAPI {
    var version: String? = null
    var generated = 0f
    var host: String? = null
    var radar: Radar? = null
    var satellite: Satellite? = null
}

class Satellite {
    var infrared = ArrayList<Any>()
}

class Radar {
    var past = ArrayList<Cast>()
    var nowcast = ArrayList<Cast>()
}

class Cast {
    var time: Long? = null
    var path: String? = null
}