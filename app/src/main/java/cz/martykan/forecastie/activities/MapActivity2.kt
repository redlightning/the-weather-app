package cz.martykan.forecastie.activities

import android.annotation.SuppressLint
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.widget.SeekBar
import androidx.lifecycle.ViewModelProvider
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.gson.Gson
import cz.martykan.forecastie.R
import cz.martykan.forecastie.utils.UI
import cz.martykan.forecastie.viewmodels.MapViewModel
import cz.martykan.forecastie.weatherapi.WeatherStorage
import cz.martykan.forecastie.weatherapi.rainviewer.RainViewerAPI
import cz.martykan.forecastie.weatherapi.rainviewer.RainViewerJsonWorker
import java.net.MalformedURLException
import java.net.URL
import java.util.*


class MapActivity2 : BaseActivity(), OnMapReadyCallback {
    private val webView: WebView? = null
    private val transparencyBar: SeekBar? = null
    private var mMap: GoogleMap? = null
    private var mapViewModel: MapViewModel? = null
    private var weatherStorage: WeatherStorage? = null
    private var rainViewerAPI: RainViewerAPI? = null
    private var radarOverlay: TileOverlay? = null

    @SuppressLint("SetJavaScriptEnabled", "AddJavascriptInterface")
    override fun onCreate(savedInstanceState: Bundle?) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_map)
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
        setTheme(UI.getTheme(prefs.getString("theme", "fresh")).also { theme = it })
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        weatherStorage = WeatherStorage(this)
        if (savedInstanceState == null) {
            mapViewModel!!.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
            mapViewModel!!.mapLat = weatherStorage!!.getLatitude(0.0)
            mapViewModel!!.mapLon = weatherStorage!!.getLongitude(0.0)
        }


    }

    private fun setMapState(item: Int) {
        when (item) {
            R.id.map_clouds -> webView!!.loadUrl(
                "javascript:map.removeLayer(rainLayer);map.removeLayer(windLayer);map.removeLayer(tempLayer);"
                        + "map.addLayer(cloudsLayer);"
            )
            R.id.map_rain -> webView!!.loadUrl(
                "javascript:map.removeLayer(cloudsLayer);map.removeLayer(windLayer);map.removeLayer(tempLayer);"
                        + "map.addLayer(rainLayer);"
            )
            R.id.map_wind -> webView!!.loadUrl(
                "javascript:map.removeLayer(cloudsLayer);map.removeLayer(rainLayer);map.removeLayer(tempLayer);"
                        + "map.addLayer(windLayer);"
            )
            R.id.map_temperature -> webView!!.loadUrl(
                "javascript:map.removeLayer(cloudsLayer);map.removeLayer(windLayer);map.removeLayer(rainLayer);"
                        + "map.addLayer(tempLayer);"
            )
            else -> Log.w("MapActivity", "Layer not configured")
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap!!.mapType = GoogleMap.MAP_TYPE_TERRAIN
        // Move the camera
        val location = LatLng(mapViewModel!!.mapLat, mapViewModel!!.mapLon)
        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(location))
        addTiles()
    }

    private fun addTiles() {
        //Get weather layers
        val task = OneTimeWorkRequest.Builder(RainViewerJsonWorker::class.java)
            .build()
        val workManager = WorkManager.getInstance(this)
        workManager.enqueueUniqueWork("get layers", ExistingWorkPolicy.KEEP, task)
        workManager.getWorkInfoByIdLiveData(task.id)
            .observe(this) {
                it?.let {
                    if (it.state.isFinished) {
                        val jsonString = it.outputData.getString(RainViewerJsonWorker.KEY_JSON)
                        val gson = Gson()
                        rainViewerAPI = gson.fromJson(jsonString, RainViewerAPI::class.java)

                        val tileProvider: TileProvider = object : UrlTileProvider(256, 256) {
                            @Synchronized
                            override fun getTileUrl(x: Int, y: Int, zoom: Int): URL {
                                val completeURL = rainViewerAPI!!.host + rainViewerAPI!!.radar!!.past[0].path + WEATHER_TILE_URL_FORMAT
                                val s = java.lang.String.format(Locale.US, completeURL, zoom, x, y)
                                val url = try {
                                    URL(s)
                                } catch (e: MalformedURLException) {
                                    throw java.lang.AssertionError(e)
                                }
                                return url
                            }
                        }
                        radarOverlay = mMap!!.addTileOverlay(TileOverlayOptions().tileProvider(tileProvider))
                    }
                }
            }
    }

    private inner class HybridInterface {
        @JavascriptInterface
        fun transferLatLon(lat: Double, lon: Double) {
            mapViewModel!!.mapLat = lat
            mapViewModel!!.mapLon = lon
        }

        @JavascriptInterface
        fun transferZoom(level: Int) {
            mapViewModel!!.mapZoom = level
        }
    }

    companion object {
        private const val TRANSPARENCY_MAX = 100
        private const val TAG = "MapActivity2"
        // Format: "/{size}/{z}/{x}/{y}/{color}/{smooth}_{snow}.png"
        private const val WEATHER_TILE_URL_FORMAT = "/256/%d/%d/%d/8/1_1.png"
    }
    /* Value Options -------------------
    @see https://www.rainviewer.com/api/weather-maps-api.html#howToUse

    Size: 256 or 512

    Colors:
    0	BW Black and White: dBZ values
    1	Original
    2	Universal Blue
    3	TITAN
    4	The Weather Channel (TWC)
    5	Meteored
    6	NEXRAD Level III
    7	Rainbow @ SELEX-IS
    8	Dark Sky

    {options} – list of options separated by the _ symbol. For example: ‘1_0’ means smoothed (1) image without snow color scheme (0). Now two options are available: {smooth}_{snow}
        {smooth} - blur (1) or not (0) radar data. Large composite images are always not smoothed due to performance issues.
        {snow} - display (1) or not (0) snow in separate colors on the tiles.
    */
}